package fr.epita.tp6.exposition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.epita.tp6.application.CalculService;

@RestController
@RequestMapping("/calcul")
public class CalculController {
	
	@Autowired
	CalculService service;
	
	@GetMapping("/add/{a}/{b}")
	public int add(@PathVariable("a") int a, @PathVariable("b") int b) {
		
		return service.add(a, b);
		
	}
	
	@GetMapping("/soust/{a}/{b}")
	public int soust(@PathVariable("a") int a, @PathVariable("b") int b) {
		return service.soust(a, b);
	}
	
	@GetMapping("/mult/{a}/{b}")
	public int mult(@PathVariable("a") int a, @PathVariable("b") int b) {
		return service.mult(a, b);
	}
	
	@GetMapping("/div/{a}/{b}")
	public double div(@PathVariable("a") int a, @PathVariable("b") int b) {
		if(b==0) {
			throw new ArithmeticException();
		}
		return service.div(a, b);
	}
	
	@GetMapping("/carre/{a}")
	public int carre(@PathVariable("a") int a, @PathVariable("a")int b) {
		System.out.println("b = "+b);
		return service.carre(a);
	}

}
