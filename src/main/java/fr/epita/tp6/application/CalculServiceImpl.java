package fr.epita.tp6.application;

import org.springframework.stereotype.Service;

@Service
public class CalculServiceImpl implements CalculService {

	public int add(int a, int b) {
		
		return a+b;
	}

	public int soust(int a, int b) {
		
		return a-b;
	}

	public int mult(int a, int b) {
		
		return a*b;
	}

	public double div(int a, int b) {
	
		return a/b;
	}

	public int carre(int a) {
		
		return a*a;
	}

}
