package fr.epita.tp6.application;

public interface CalculService {
	
	int add(int a, int b);
	int soust(int a, int b);
	int mult(int a, int b);
	double div(int a, int b);
	int carre(int a);

}
