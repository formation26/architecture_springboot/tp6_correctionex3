# Exercice 3 

---

Développer une calculatrice

Les opérations sont données dans l’URL

Le résultat de 2+3 est donnée par <http://localhost:8080/calcul/add/2/3>   
Le résultat de 2*3 est donnée par <http://localhost:8080/calcul/mult/2/3>  
Le résultat de 2/3 est donnée par <http://localhost:8080/calcul/div/2/3>  
	> *Attention*:   vérifier que le dénominateur soit différent de 0  
Le résultat de 2-3 est donnée par <http://localhost:8080/calcul/soust/2/3>   
Le résultat de 3<sup>2</sup> est donnée par <http://locahost:8080/calcul/carre/3>   